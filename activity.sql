-- 1.
SELECT customerName FROM customers WHERE country = "Philippines";

-- 2.
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 3.
SELECT productName, MSRP  FROM products WHERE productName = "The Titanic";

-- 4.
SELECT firstName, lastName  FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 5.
SELECT customerName  FROM customers WHERE state IS NULL ;

-- 6.
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "steve" ;

-- 7.
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000 ;

-- 8.
SELECT customerNumber, comments FROM orders WHERE comments LIKE "%DHL%" ;

-- 9. Return the product lines whose text description mentions the phrase 'state of the art'
SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";


-- 10. Return the countries of customers without duplication
SELECT DISTINCT country FROM customers;


-- 11. Return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;

-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName, country FROM customers WHERE country IN ("USA", "FRANCE", "CANADA");

-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo

SELECT firstName, lastName, city FROM employees LEFT JOIN offices ON employees.officeCode = offices.officeCode WHERE city = "Tokyo";

-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
SELECT customerName FROM customers LEFT JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE firstName ="Leslie" AND lastName = "Thompson";

-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"

SELECT productName, customerName
	FROM orders 
	JOIN customers ON orders.customerNumber = customers.customerNumber 
	JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
	JOIN products ON orderdetails.productCode = products.productCode
	WHERE customerName = "Baane Mini Imports" ;


-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
SELECT firstName, lastName, customerName, offices.country
	FROM customers 
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber 
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE customers.country = offices.country ;


-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.
SELECT productName, quantityInStock
	FROM products 
	WHERE productLine = "Planes" 
	AND  quantityInStock < 1000;

-- 18. Return the customer's name with a phone number containing "+81".

SELECT customerName FROM customers WHERE phone LIKE "%+81%";